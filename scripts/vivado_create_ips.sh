#!/bin/bash

# Disable wildcard expansion because of the possibility of a '*' as
# ip-name-pattern.
set -o noglob

TCL_ARGS=""

if [ ! -z "$1" ]; then
    TCL_ARGS="-ip-name-pattern $1"
fi

if [ ! -z "$2" ]; then
    TCL_ARGS="$TCL_ARGS -target-part $2"
fi

TCL_ARGS_STR=""
if [ ! -z "${TCL_ARGS}" ]; then
    TCL_ARGS_STR="-tclargs ${TCL_ARGS}"
fi

vivado -mode batch -notrace -nolog -nojou -quiet -source scripts/vivado_create_ips.tcl ${TCL_ARGS_STR}
