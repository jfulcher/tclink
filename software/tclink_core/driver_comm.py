#!/usr/bin/env python

# Copyright CERN 2020.
# This source describes Open Hardware and is licensed under the CERN-OHLW v2
# You may redistribute and modify this documentation and make products
# using it under the terms of the CERN-OHL-W v2 (https:/cern.ch/cern-ohl).
# This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED
# WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY
# AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-W v2
# for applicable conditions.
# Source location: https://gitlab.cern.ch/HPTD/tclink
# As per CERN-OHL-W v2 section 4.1, should You produce hardware based on
# these sources, You must maintain the Source Location visible on the
# external case of the TCLink or other product you make using
# this documentation.
#

import socket
import logging

# -------------------------------------------------------------
#  -------------- Class DriverComm (driver-layer) ------------
# -------------------------------------------------------------


class DriverComm:

    def __init__(self, ip='192.168.1.2', port=9001, logger_name=None):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((ip, port))
        self.logger = logging.getLogger(logger_name)
        self.logger.debug('Opening socket at port:' + str(port) + ", ip:" + ip)

    def send(self, data):
        data=data.encode('utf-8')
        self.s.send(data)

    def recv(self):
        r = ' '
        data = ''
        while r != '\n':
            data += self.s.recv(4 * 1024)
            r = data[-1]
        return data[:-1]

    def query(self, data):
        self.send(data)
        return self.s.recv(4096)