########################################################################

# These describe the IP core to be built.
set ip_name vio
set ip_vendor xilinx.com
set ip_library ip
set ip_version 3.0

# This is the name to give to the generated module.
set module_name vio_control_kcu105

# These are the settings to apply to the generated module.
set module_properties {
    CONFIG.C_PROBE_OUT48_INIT_VAL {0x1}
    CONFIG.C_PROBE_OUT47_INIT_VAL {0x000003ffa41a}
    CONFIG.C_PROBE_OUT45_INIT_VAL {0x000}
    CONFIG.C_PROBE_OUT43_INIT_VAL {0x14}
    CONFIG.C_PROBE_OUT42_INIT_VAL {0x002}
    CONFIG.C_PROBE_OUT40_INIT_VAL {0x0000000ffe90}
    CONFIG.C_PROBE_OUT39_INIT_VAL {0x1}
    CONFIG.C_PROBE_OUT38_INIT_VAL {0x0197}
    CONFIG.C_PROBE_OUT37_INIT_VAL {0xE}
    CONFIG.C_PROBE_OUT34_INIT_VAL {0x00007ff48348}
    CONFIG.C_PROBE_OUT33_INIT_VAL {0x040}
    CONFIG.C_PROBE_OUT32_INIT_VAL {0x0052}
    CONFIG.C_PROBE_OUT31_INIT_VAL {0x000000000000}
    CONFIG.C_PROBE_OUT29_INIT_VAL {0x1}
    CONFIG.C_PROBE_OUT28_INIT_VAL {0x1}
    CONFIG.C_PROBE_OUT19_INIT_VAL {0x00}
    CONFIG.C_PROBE_OUT18_INIT_VAL {0x0}
    CONFIG.C_PROBE_OUT17_INIT_VAL {0x0000}
    CONFIG.C_PROBE_OUT16_INIT_VAL {0x000}
    CONFIG.C_PROBE_OUT8_INIT_VAL {0x1}
    CONFIG.C_PROBE_OUT7_INIT_VAL {0x1}
    CONFIG.C_PROBE_OUT1_INIT_VAL {0x1}
    CONFIG.C_PROBE_OUT0_INIT_VAL {0x1}
    CONFIG.C_PROBE_OUT58_WIDTH {10}
    CONFIG.C_PROBE_OUT56_WIDTH {3}
    CONFIG.C_PROBE_OUT47_WIDTH {48}
    CONFIG.C_PROBE_OUT45_WIDTH {10}
    CONFIG.C_PROBE_OUT43_WIDTH {5}
    CONFIG.C_PROBE_OUT42_WIDTH {10}
    CONFIG.C_PROBE_OUT41_WIDTH {1}
    CONFIG.C_PROBE_OUT40_WIDTH {48}
    CONFIG.C_PROBE_OUT39_WIDTH {1}
    CONFIG.C_PROBE_OUT38_WIDTH {16}
    CONFIG.C_PROBE_OUT37_WIDTH {4}
    CONFIG.C_PROBE_OUT36_WIDTH {1}
    CONFIG.C_PROBE_OUT35_WIDTH {4}
    CONFIG.C_PROBE_OUT34_WIDTH {48}
    CONFIG.C_PROBE_OUT33_WIDTH {12}
    CONFIG.C_PROBE_OUT32_WIDTH {16}
    CONFIG.C_PROBE_OUT31_WIDTH {48}
    CONFIG.C_PROBE_OUT23_WIDTH {4}
    CONFIG.C_PROBE_OUT19_WIDTH {7}
    CONFIG.C_PROBE_OUT18_WIDTH {1}
    CONFIG.C_PROBE_OUT17_WIDTH {16}
    CONFIG.C_PROBE_OUT16_WIDTH {10}
    CONFIG.C_PROBE_OUT13_WIDTH {4}
    CONFIG.C_PROBE_OUT11_WIDTH {4}
    CONFIG.C_PROBE_OUT9_WIDTH {3}
    CONFIG.C_PROBE_IN35_WIDTH {22}
    CONFIG.C_PROBE_IN33_WIDTH {10}
    CONFIG.C_PROBE_IN32_WIDTH {3}
    CONFIG.C_PROBE_IN30_WIDTH {1}
    CONFIG.C_PROBE_IN29_WIDTH {16}
    CONFIG.C_PROBE_IN27_WIDTH {16}
    CONFIG.C_PROBE_IN26_WIDTH {48}
    CONFIG.C_PROBE_IN25_WIDTH {32}
    CONFIG.C_PROBE_IN24_WIDTH {5}
    CONFIG.C_PROBE_IN21_WIDTH {16}
    CONFIG.C_PROBE_IN19_WIDTH {32}
    CONFIG.C_PROBE_IN18_WIDTH {7}
    CONFIG.C_PROBE_IN17_WIDTH {16}
    CONFIG.C_NUM_PROBE_OUT {62}
    CONFIG.C_NUM_PROBE_IN {43}
}

# Do we need the example design as well?
set include_example_design false

# The default part and evaluation board to target (unless overridden
# from the command line).
set default_part "xcku040-ffva1156-2-e"
set default_board ""

########################################################################

package require vivado_utils

vivado_utils::run_vivado_create_ip \
    $ip_name $ip_vendor $ip_library $ip_version \
    $module_name $module_properties \
    $include_example_design \
    $default_part $default_board \
    $::argv

########################################################################
