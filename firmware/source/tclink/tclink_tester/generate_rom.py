#!/usr/bin/env python

# Copyright CERN 2020.
# This source describes Open Hardware and is licensed under the CERN-OHLW v2
# You may redistribute and modify this documentation and make products
# using it under the terms of the CERN-OHL-W v2 (https:/cern.ch/cern-ohl).
# This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED
# WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY
# AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-W v2
# for applicable conditions.
# Source location: https://gitlab.cern.ch/HPTD/tclink
# As per CERN-OHL-W v2 section 4.1, should You produce hardware based on
# these sources, You must maintain the Source Location visible on the
# external case of the TCLink or other product you make using
# this documentation.
#==============================================================================
# Author: EBSM - CERN EP/ESE
# Date: 14/11/2019
#==============================================================================

#-----------------------------------------------------------------
#---                  Python Native Packages                   ---
#-----------------------------------------------------------------
import math
import matplotlib.pyplot as plt

rom_size  = 2**10  ;
width     = 32     ;
amplitude = 2**31-1; 

rom = '('
for i in range(0,rom_size):
    signal = amplitude*math.sin(2*math.pi*i/rom_size)
    signal = round(signal);
    if(signal >= 0): str_signal = 'x"%08x"' % signal
    else          : str_signal = 'x"%08x"' % ((abs(signal) ^ 0xFFFFFFFF) + 1)
    if(i==rom_size-1) : rom = rom + str_signal + ');'
    else              : rom = rom + str_signal + ', '

print(rom)

# Check data visually
data = []
for i in range(0,rom_size):
    if(rom[i] >= 2**31) : data.append((rom[i] ^ 0xFFFFFFFF) + 1)
    else : data.append(rom[i])

plt.plot(data)
plt.show()