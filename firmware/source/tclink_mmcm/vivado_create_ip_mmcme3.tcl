########################################################################

# These describe the IP core to be built.
set ip_name clk_wiz
set ip_vendor xilinx.com
set ip_library ip
set ip_version 6.0

# This is the name to give to the generated module.
set module_name mmcme3

# These are the settings to apply to the generated module.
set module_properties {
    CONFIG.PRIM_IN_FREQ {320.632}
    CONFIG.PHASESHIFT_MODE {LATENCY}
    CONFIG.CLKIN1_JITTER_PS {31.18}
    CONFIG.CLKOUT1_USED {true}
    CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {320.0}
    CONFIG.RESET_PORT {resetn}
    CONFIG.MMCM_DIVCLK_DIVIDE {22}
    CONFIG.MMCM_CLKFBOUT_MULT_F {63.125}
    CONFIG.MMCM_CLKIN1_PERIOD {3.119}
    CONFIG.MMCM_CLKIN2_PERIOD {10.0}
    CONFIG.MMCM_CLKOUT0_DIVIDE_F {2.875}
    CONFIG.RESET_TYPE {ACTIVE_LOW}
    CONFIG.CLKOUT1_JITTER {271.801}
    CONFIG.CLKOUT1_PHASE_ERROR {509.381}
}

# Do we need the example design as well?
set include_example_design false

# The default part and evaluation board to target (unless overridden
# from the command line).
set default_part "xcku040-ffva1156-2-e"
set default_board "xilinx.com:kcu105:part0:1.6"

########################################################################

package require vivado_utils

vivado_utils::run_vivado_create_ip \
    $ip_name $ip_vendor $ip_library $ip_version \
    $module_name $module_properties \
    $include_example_design \
    $default_part $default_board \
    $::argv

########################################################################
