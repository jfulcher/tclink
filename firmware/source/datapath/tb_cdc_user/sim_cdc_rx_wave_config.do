add wave -position insertpoint  \
sim:/tb_cdc_rx/c_PRBS_POLYNOMIAL \
sim:/tb_cdc_rx/c_STROBE_PERIOD \
sim:/tb_cdc_rx/clk_a \
sim:/tb_cdc_rx/clk_b \
sim:/tb_cdc_rx/data_a \
sim:/tb_cdc_rx/data_b \
sim:/tb_cdc_rx/g_CLOCK_A_RATIO \
sim:/tb_cdc_rx/g_CLOCK_B_RATIO \
sim:/tb_cdc_rx/phase \
sim:/tb_cdc_rx/phase_calib \
sim:/tb_cdc_rx/phase_force \
sim:/tb_cdc_rx/prbschk_data_chk \
sim:/tb_cdc_rx/prbschk_error \
sim:/tb_cdc_rx/prbschk_locked \
sim:/tb_cdc_rx/prbschk_reset \
sim:/tb_cdc_rx/prbsgen_data \
sim:/tb_cdc_rx/prbsgen_data_valid \
sim:/tb_cdc_rx/prbsgen_en \
sim:/tb_cdc_rx/prbsgen_load \
sim:/tb_cdc_rx/prbsgen_reset \
sim:/tb_cdc_rx/prbsgen_seed \
sim:/tb_cdc_rx/reset_a \
sim:/tb_cdc_rx/save_file \
sim:/tb_cdc_rx/strobe_a \
sim:/tb_cdc_rx/ready_b \
sim:/tb_cdc_rx/strobe_b